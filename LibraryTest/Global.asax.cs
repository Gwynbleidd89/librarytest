﻿using LibraryTest.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace LibraryTest
{
    public class MvcApplication : System.Web.HttpApplication
    {
        const uint FIFTEEN = 900000;

        // Создаем экзмпеляр службы автоматической отмены бронирования
        /// <summary>
        /// Экземпляр службы автоматической отмены бронирования.
        /// </summary>
        ReserveCancellationService RCS = new ReserveCancellationService();

        //public static RequestContext RequestContext;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Запускам службу если она не запущена
            if (!RCS.Status)
            {
                RCS.StartService(FIFTEEN, FIFTEEN);
            }      
        }

        protected void Application_Disposed()
        { 
            // Останавливаем и очищаем ресурсы службы
            RCS.Dispose();
        }

    }

    /// <summary>
    /// Служба автоматической отмены бронирования.
    /// </summary>
    public class ReserveCancellationService : IDisposable
    {
        public ReserveCancellationService()
        {
            _timerCB = new TimerCallback(CheckAndCancelAsync);
        }

        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Ставит накладывающиеся потоки в очередь, хотя было бы неплохо их завершать.
        /// </summary>
        private SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1);

        /// <summary>
        /// Метод обратного вызова.
        /// </summary>
        private TimerCallback _timerCB;

        /// <summary>
        /// Таймер пула потоков данной службы.
        /// </summary>
        private System.Threading.Timer _timer;

        private bool _status;

        /// <summary>
        /// Возвращает статус работы службы.
        /// </summary>
        public bool Status { get => _status; }

        /// <summary>
        /// Запускает службу.
        /// </summary>
        /// <param name="Period">Интервал в миллисекундах.</param>
        /// <param name="WaitUntilStart">Время ожидания до первой итерации в миллисекундах.</param>
        public void StartService(uint WaitUntilStart, uint Period)
        {
            if (_timer != null)
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
                _timer = null;
            }
            _timer = new Timer(_timerCB, null, WaitUntilStart, Period);
            _status = true;
        }

        /// <summary>
        /// Останавливает службу.
        /// </summary>
        public void StopService()
        {
            if (_timer != null)
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
            }
            _status = false;
        }

        /// <summary>
        /// Проверяет забронированные книги и если срок бронирования истек
        /// то бронированние ануллируется, после чего уведомляет всех пытающихся сделать бронь о доступности книг.
        /// </summary>
        public async void CheckAndCancelAsync(object obj)
        {
            try
            {
                await _semaphoreSlim.WaitAsync();
                // Получаем список книг, которые не являются выданными, а также с истекшим сроком бронирования
                IQueryable<BooksModel> books =
                        db.Books.Where(b => b.ApplicationUser != null && b.IssuingStatus == false && b.ReservationEnd != null && b.ReservationEnd < DateTime.Now);
                // Получаем список попыток бронирования книг, которые не являются выданными, а также истек срок бронирования
                IQueryable<RecentAttemptsReservationModel> recentAttemptsReservationModel =
                    db.RecentAttemptsReservationModels.Include(r => r.ApplicationUser).Include(r => r.Books).Where(r => r.ApplicationUser != null
                       && r.Books != null && r.Books.IssuingStatus == false && r.Books.ReservationEnd != null && r.Books.ReservationEnd < DateTime.Now);
                List<RecentAttemptsReservationModel> RARM = await recentAttemptsReservationModel.ToListAsync();
                // Аннулируем бронирование книг и сохраняем изменения
                foreach (BooksModel book in books)
                {
                    book.ApplicationUser = null;
                    book.ApplicationUserId = null;
                    book.ReservationEnd = null;
                }
                await db.SaveChangesAsync();
                // Если количество попыток - ноль то выходим из метода
                if (RARM.Count() < 1)
                {
                    return;
                }
                // Словарь имен книг, которые пытались забронировать пользователи
                Dictionary<string, List<string>> UserBooks = new Dictionary<string, List<string>>();
                // Заполняем словарь: пользователь | список книг
                foreach (RecentAttemptsReservationModel Item_RARM in RARM)
                {
                    if (Item_RARM.ApplicationUser == null || Item_RARM.ApplicationUserId == null || Item_RARM.Books == null)
                    {
                        break;
                    }
                    // Чтобы я не делал получаю какое либо исключение контекста запроса, даже создав синхронный метод, потому ссылок не будет
                    // Формируем ссылку на книгу и оборачиваем ее в элемент списка
                    /*string href = HtmlHelper.GenerateLink
                        (
                            requestContext: MvcApplication.RequestContext,//this.RequestContext,
                            routeCollection: RouteTable.Routes,
                            routeName: "Default",
                            linkText: Item_RARM.Books.Name,
                            actionName: "Details",
                            controllerName: "JustUserBooks",
                            routeValues: new RouteValueDictionary(new { id = Item_RARM.Books.Id }),
                            htmlAttributes: null
                        );
                    string li = "<li>" + href + "</li>";*/
                    string li = "<li>" + Item_RARM.Books.Name + "</li>";
                    string Recipient = Item_RARM.ApplicationUser.Email;
                    // Если ключ уже присутствует то добавляем элемент списка в список, иначе создаем запись: пользователь | список книг
                    if (UserBooks.ContainsKey(Recipient))
                    {
                        UserBooks[Recipient].Add(li);
                    }
                    else
                    {
                        UserBooks.Add(Recipient, new List<string> { li });
                    }
                }
                // Удаляем полученные записи о попытках бронирования из сущности
                db.RecentAttemptsReservationModels.RemoveRange(RARM);
                // Сохраняем изменения в сущности
                await db.SaveChangesAsync();
                // Отправляем сообщения со списком доступных книг, которые пользователи пытались забронировать
                await GeneralMethods.SendEmailRecentAttemptsReservationAsync(UserBooks);
            }
            finally
            {
                _semaphoreSlim.Release();
            }
        }

        public void Dispose()
        {
            _semaphoreSlim.Dispose();
            db.Dispose();
            if (_timer != null)
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
                _timer.Dispose();
            }
        }
    }
}
