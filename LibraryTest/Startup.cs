﻿using LibraryTest.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LibraryTest.Startup))]
namespace LibraryTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesandUsers();
        }
        private void CreateRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // Создаем роль админа если таковой не существует 
            if (!roleManager.RoleExists("Admin"))
            {
                IdentityRole role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                // Создаем пользователя		

                ApplicationUser user = new ApplicationUser();
                user.Email = Resources.Resource.ApplicationEmail;
                user.FirstName = "Иван";
                user.MiddleName = "Иванович";
                user.LastName = "Иванов";
                user.UserName = Resources.Resource.ApplicationEmail;
                user.EmailConfirmed = true;
                user.VerifiedByAdmin = true;

                string userPWD = Resources.Resource.ApplicationEmailPassword;

                IdentityResult chkUser = UserManager.Create(user, userPWD);

                //Добавляем роль админа пользователю
                if (chkUser.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Admin");
                }
            }

            // Создаем роль библиотекаря если таковой не существует
            if (!roleManager.RoleExists("Librarian"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Librarian";
                roleManager.Create(role);

                // Создаем пользователя

                ApplicationUser user = new ApplicationUser();
                user.Email = "librarytest@mail.ru";
                user.FirstName = "Петр";
                user.MiddleName = "Петровович";
                user.LastName = "Петров";
                user.UserName = "librarytest@mail.ru";
                user.EmailConfirmed = true;
                user.VerifiedByAdmin = true;

                string userPWD = "qwerty123";

                IdentityResult chkUser = UserManager.Create(user, userPWD);

                //Добавляем роль библиотекаря пользователю
                if (chkUser.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Librarian");
                }
            }

            // Создаем роль обычного пользователя если таковой не существует
            if (!roleManager.RoleExists("JustUser"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "JustUser";
                roleManager.Create(role);

                // Создаем пользователей

                ApplicationUser user1 = new ApplicationUser();
                user1.Email = "librarytest1@mail.ru";
                user1.FirstName = "Валерий";
                user1.MiddleName = "Валериевич";
                user1.LastName = "Валерьев";
                user1.UserName = "librarytest1@mail.ru";
                user1.EmailConfirmed = true;
                user1.VerifiedByAdmin = true;

                ApplicationUser user2 = new ApplicationUser();
                user2.Email = "librarytest2@mail.ru";
                user2.FirstName = "Николай";
                user2.MiddleName = "Николаевич";
                user2.LastName = "Николаев";
                user2.UserName = "librarytest2@mail.ru";
                user2.EmailConfirmed = true;
                user2.VerifiedByAdmin = true;

                string userPWD = "qwerty123";

                IdentityResult chkUser = UserManager.Create(user1, userPWD);

                //Добавляем роль пользователя пользователю
                if (chkUser.Succeeded)
                {
                    UserManager.AddToRole(user1.Id, "JustUser");
                }

                chkUser = UserManager.Create(user2, userPWD);

                //Добавляем роль пользователя пользователю
                if (chkUser.Succeeded)
                {
                    UserManager.AddToRole(user2.Id, "JustUser");
                }
            }

            UserManager.Dispose();
            roleManager.Dispose();
            context.Dispose();
        }
    }

    

}
