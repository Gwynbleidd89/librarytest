﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;

namespace LibraryTest.Models
{

    public class LibrarianBooksViewModel
    {

        [Required]
        public List<BooksModel> Books { get; set; }

        [StringLength(maximumLength: 150, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        public string FilterValue { get; set; }

        /// <summary>
        /// Перечисление фильтров.
        /// </summary>
        public enum FilterParameters
        {
            /// <summary>
            /// Фильтр "автор".
            /// </summary>
            [Display(Name = "автору")]
            Author = 1,
            /// <summary>
            /// Фильтр "жанр".
            /// </summary>
            [Display(Name = "жанру")]
            Genre = 2,
            /// <summary>
            /// Фильтр "издатель".
            /// </summary>
            [Display(Name = "издателю")]
            Publisher = 3,
            /// <summary>
            /// Фильтр "наименование".
            /// </summary>
            [Display(Name = "наименованию")]
            Name = 4,
            /// <summary>
            /// Фильтр "бронирующий".
            /// </summary>
            [Display(Name = "бронирующему")]
            User_FullName_Or_Email = 5,
        };

        /// <summary>
        /// Перечисление параметров статуса бронирования.
        /// </summary>
        public enum ReservationStatusParameters
        {
            /// <summary>
            /// Статус "Любой".
            /// </summary>
            [Display(Name = "Любой")]
            All = 1,
            /// <summary>
            /// Статус "Доступные".
            /// </summary>
            [Display(Name = "Доступные")]
            Available = 2,
            /// <summary>
            /// Статус "Забронированные".
            /// </summary>
            [Display(Name = "Забронированные")]
            Reserved = 3,
            /// <summary>
            /// Статус "Выданные".
            /// </summary>
            [Display(Name = "Выданные")]
            Issued = 4
        };

        private FilterParameters _filterParameter = FilterParameters.Author;
        [Display(Name = "Фильтровать по:")]
        public FilterParameters FilterParameter { get => _filterParameter; set => _filterParameter = value; }

        private ReservationStatusParameters _reservationStatusParameter = ReservationStatusParameters.All;
        [Display(Name = "Статус бронирования:")]
        public ReservationStatusParameters ReservationStatusParameter
        {
            get => _reservationStatusParameter;
            set => _reservationStatusParameter = value;
        }

    }

    public class LibrarianCreateEditBooksViewModel
    {

        /// <summary>
        /// Задает или возвращает идентификатор книги.
        /// </summary>
        public int Id { get; set; }

        [Display(Name = "DN_Name", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Name",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 150, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает наименование книги.
        /// </summary>
        public string Name { get; set; }

        [Display(Name = "DN_Author", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Author",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 150, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает автора книги.
        /// </summary>
        public string Author { get; set; }

        [Display(Name = "DN_Ganre", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Ganre",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает жанр книги.
        /// </summary>
        public string Ganre { get; set; }

        [Display(Name = "DN_Publisher", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Publisher",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или издателя книги.
        /// </summary>
        public string Publisher { get; set; }

    }
}