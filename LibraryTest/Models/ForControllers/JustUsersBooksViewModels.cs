﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryTest.Models
{

    public class JustUsersBooksViewModel
    {

        [Required]
        public List<BooksModel> Books { get; set; }

        [StringLength(maximumLength: 150, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        public string FilterValue { get; set; }

        /// <summary>
        /// Перечисление фильтров.
        /// </summary>
        public enum FilterParameters
        {
            /// <summary>
            /// Фильтр "автор".
            /// </summary>
            [Display(Name = "автору")]
            Author = 1,
            /// <summary>
            /// Фильтр "жанр".
            /// </summary>
            [Display(Name = "жанру")]
            Genre = 2,
            /// <summary>
            /// Фильтр "издатель".
            /// </summary>
            [Display(Name = "издателю")]
            Publisher = 3,
            /// <summary>
            /// Фильтр "наименование".
            /// </summary>
            [Display(Name = "наименованию")]
            Name = 4
        };

        /// <summary>
        /// Перечисление параметров статуса бронирования.
        /// </summary>
        public enum ReservationStatusParameters
        {
            /// <summary>
            /// Статус "Любой".
            /// </summary>
            [Display(Name = "Любой")]
            All = 1,
            /// <summary>
            /// Статус "Доступные".
            /// </summary>
            [Display(Name = "Доступные")]
            Available = 2,
            /// <summary>
            /// Статус "Забронированные".
            /// </summary>
            [Display(Name = "Забронированные")]
            Reserved = 3,
            /// <summary>
            /// Статус "Забронированные мной".
            /// </summary>
            [Display(Name = "Забронированные мной")]
            ReservedForMe = 4,
            /// <summary>
            /// Статус "Выданные мне".
            /// </summary>
            [Display(Name = "Выданные мне")]
            IssuedToMe = 5
        };
        
        private FilterParameters _filterParameter = FilterParameters.Author;
        [Display(Name = "Фильтровать по:")]
        public FilterParameters FilterParameter { get => _filterParameter; set => _filterParameter = value; }

        private ReservationStatusParameters _reservationStatusParameter = ReservationStatusParameters.All;
        [Display(Name = "Статус бронирования:")]
        public ReservationStatusParameters ReservationStatusParameter
        {
            get => _reservationStatusParameter;
            set => _reservationStatusParameter = value;
        }
    }

    public class CommentCreateViewModel
    {
        /// <summary>
        /// Перечисление оценок.
        /// </summary>
        public enum RatingParameters
        {
            /// <summary>
            /// Оценка "ужасно".
            /// </summary>
            [Display(Name = "Ужасно")]
            One = 1,
            /// <summary>
            /// Оценка "плохо".
            /// </summary>
            [Display(Name = "Плохо")]
            Two = 2,
            /// <summary>
            /// Оценка "нейтрально".
            /// </summary>
            [Display(Name = "Нейтрально")]
            Three = 3,
            /// <summary>
            /// Оценка "хорошо".
            /// </summary>
            [Display(Name = "Хорошо")]
            Four = 4,
            /// <summary>
            /// Оценка "отлично".
            /// </summary>
            [Display(Name = "Отлично")]
            Five = 5
        };

        //For default value
        private RatingParameters _ratingParameter = RatingParameters.Three;

        [Display(Name = "Ваша оценка:")]
        public RatingParameters RatingParameter { get => _ratingParameter; set => _ratingParameter = value; }

        [Display(Name = "DN_Text", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Text",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 500, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [DataType(DataType.MultilineText)]
        public string CommentText { get; set; }
    }

    public class BookDetailsModel
    {
        [Required]
        public BooksModel Book { get; set; }

        public CommentsModel CurrentUserComment { get; set; }

        [Required]
        public List<CommentsModel> AllComments { get; set; }

        [Display(Name = "Оценка")]
        public string GeneralRating { get; set; }
    }

}