﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryTest.Models
{

    public class UsersViewModel
    {
        [Required]
        public List<ApplicationUser> ApplicationUsers { get; set; }

        [Display(Name = "Поисковое значение:")]
        [StringLength(maximumLength: 150, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        public string FilterValue { get; set; }

    }

    public class CreateEditUserViewModel
    {
        public string UserID { get; set; }

        [Display(Name = "DN_FirstName", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_FirstName",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        //[RegularExpression(namesRegex, ErrorMessageResourceName = "RegexMismatch",
        //ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает имя пользователя.
        /// </summary>
        public string FirstName { get; set; }

        [Display(Name = "DN_LastName", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_LastName",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        //[RegularExpression(namesRegex, ErrorMessageResourceName = "RegexMismatch",
        //ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает фамилию пользователя.
        /// </summary>
        public string LastName { get; set; }

        [Display(Name = "DN_MiddleName", ResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        //[RegularExpression(namesRegex, ErrorMessageResourceName = "RegexMismatch",
        //ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает отчество пользователя.
        /// </summary>
        public string MiddleName { get; set; }

        [Display(Name = "DN_Email", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Email",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 255, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        //public IEnumerable<SelectListItem> RoleValue { get; set; }
        
        public string RoleValue { get; set; }

        [Display(Name = "Роль")]
        public SelectList Role { get; set; }
    }
}