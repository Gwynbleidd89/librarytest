﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Threading.Tasks;

namespace LibraryTest.Models
{
    public static class Extensions
    {
        /// <summary>
        /// Возвращает Display Value в зависимости от значения параметра "b".
        /// </summary>
        /// <param name="b">Параметр Display Value которого необходимо вернуть. </param>
        /// <param name="TrueValue">Значение истины для Display Value.</param>
        /// <param name="FalseValue">Значение лжи для Display Value.</param>
        /// <returns></returns>
        public static string GetDV_String(this Boolean b, string TrueValue = "Да", string FalseValue = "Нет")
        {
            return b ? TrueValue : FalseValue;
        }

        /// <summary>
        /// Возвращает Display Value, если строка является NULL вернется параметр NullValue, иначе вернется значение строки.
        /// </summary>
        /// <param name="s">Строка Display Value которой необходимо определить.</param>
        /// <param name="NullValue">Значение, которое необходимо вернуть, если строка равна NULL.</param>
        /// <returns></returns>
        public static string GetDV_String(this string s, string NullValue = "Пустая")
        {
            return s ?? NullValue; //s == null ? NullValue : s;
        }

        /// <summary>
        /// Возвращает Display Value, если переменная является NULL вернется параметр NullValue, иначе вернется значение переменной в виде строки.
        /// </summary>
        /// <param name="s">Переменная Display Value которой необходимо определить.</param>
        /// <param name="NullValue">Значение, которое необходимо вернуть, если переменная равна NULL.</param>
        /// <returns></returns>
        public static string GetDV_String(this int? i, string NullValue = "Пустая")
        {
            return i == null ? NullValue : i.ToString();
        }

        /// <summary>
        /// Возвращает Display Namе от enumValue.
        /// </summary>
        public static string GetDN_String(this Enum enumValue)
        {
            return enumValue.GetType()
                .GetMember(enumValue.ToString())
                .First()
                .GetCustomAttribute<DisplayAttribute>()
                .GetName();
        }
    }

    public static class GeneralMethods
    {
        /// <summary>
        /// Рассылает пользователям сообщения о доступности книги для бронирования.
        /// </summary>
        /// <param name="book">Сущность книги о доступности которой необходимо разослать сообщения.</param>
        /// <returns></returns>
        public static async Task SendEmailRecentAttemptsReservationAsync(BooksModel book, ApplicationDbContext db)
        {
            if (book.RecentAttemptsReservationModels == null || book.RecentAttemptsReservationModels.Count < 1)
            {
                db.Dispose();
                return;
            }
            using (SmtpClient client = new SmtpClient(Resources.Resource.ApplicationSMTP, Convert.ToInt32(Resources.Resource.AppicationSMPT_Port)))
            {
                // Настроим клиент
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                NetworkCredential networkCredential = new NetworkCredential(Resources.Resource.ApplicationEmail, Resources.Resource.ApplicationEmailPassword);
                client.EnableSsl = true;
                client.Credentials = networkCredential;
                List<RecentAttemptsReservationModel> forDelete = new List<RecentAttemptsReservationModel>();
                foreach (RecentAttemptsReservationModel model in book.RecentAttemptsReservationModels)
                {
                    if (model.ApplicationUser == null || model.ApplicationUserId == null)
                    {
                        break;
                    }
                    if (model.ApplicationUser.Email == null || model.ApplicationUser.EmailConfirmed == false)
                    {
                        break;
                    }
                    using (MailMessage email = new MailMessage(Resources.Resource.ApplicationEmail, model.ApplicationUser.Email))
                    {
                        // Сформируем сообщение
                        string fullName = model.ApplicationUser.FirstName;
                        string longDate = model.AttemptDateTime.ToLongDateString();
                        string time = model.AttemptDateTime.TimeOfDay.Hours.ToString() + ":" + model.AttemptDateTime.TimeOfDay.Minutes.ToString();
                        if (model.ApplicationUser.MiddleName != null)
                        {
                            fullName += " " + model.ApplicationUser.MiddleName;
                        }
                        fullName += " " + model.ApplicationUser.LastName;
                        email.IsBodyHtml = true;
                        email.Subject = "Доступные книги";
                        email.Body =
                            String.Format(
                                "Уважаемый(ая), {0}, {1} в {2} вы пытались забронировать книгу: '{3}'. Уведомляем вас о том, что она снова доступна для бронирования.",
                                fullName, longDate, time, book.Name
                            );
                        await client.SendMailAsync(email);
                        forDelete.Add(model);
                    }   
                }
                if (forDelete.Count > 0)
                {
                    db.RecentAttemptsReservationModels.RemoveRange(forDelete);
                    await db.SaveChangesAsync();
                }
            }
            db.Dispose();
        }

        /// <summary>
        /// Рассылает пользователям сообщения о доступности списка книг для бронирования.
        /// </summary>
        /// <param name="UserBooks">Словарь Почта пользователь | Список книг в html теге "li".</param>
        /// <returns></returns>
        public static async Task SendEmailRecentAttemptsReservationAsync(Dictionary<string, List<string>> UserBooks)
        {
            using (SmtpClient client = new SmtpClient(Resources.Resource.ApplicationSMTP, Convert.ToInt32(Resources.Resource.AppicationSMPT_Port)))
            {
                // Настроим клиент
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                NetworkCredential networkCredential = new NetworkCredential(Resources.Resource.ApplicationEmail, Resources.Resource.ApplicationEmailPassword);
                client.EnableSsl = true;
                client.Credentials = networkCredential;
                foreach(KeyValuePair<string, List<string>> Item_UserBooks in UserBooks)
                {
                    string ul = "<ul>" + String.Join("", Item_UserBooks.Value) + "</ul>";
                    string body = @"<h3>Следующие книги, которые вы пытались забронировать стали доступны:<h3>" + ul;
                    if (Item_UserBooks.Value.Count == 1)
                    {
                        body = @"<h3>Книга которую вы пытались забронировать стала доступна:<h3>" + ul;
                    }
                    //Сформируем сообщениe
                    using (MailMessage email = new MailMessage(Resources.Resource.ApplicationEmail, Item_UserBooks.Key))
                    {
                        email.IsBodyHtml = true;
                        email.Subject = "Доступные книги";
                        email.Body = body;
                        await client.SendMailAsync(email);
                    }
                }
            }
        }
    }
}