﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LibraryTest.Models
{
    // В профиль пользователя можно добавить дополнительные данные, если указать больше свойств для класса ApplicationUser. Подробности см. на странице https://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        //public const string namesRegex = @"[^а-яА-ЯёЁ]";

        [Display(Name = "DN_VerifiedByAdmin", ResourceType = typeof(Resources.Resource))]
        [Required]
        public bool VerifiedByAdmin { get; set; }

        [Display(Name = "DN_FirstName", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_FirstName",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        //[RegularExpression(namesRegex, ErrorMessageResourceName = "RegexMismatch",
            //ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает имя пользователя.
        /// </summary>
        public string FirstName { get; set; }

        [Display(Name = "DN_LastName", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_LastName",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        //[RegularExpression(namesRegex, ErrorMessageResourceName = "RegexMismatch",
            //ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает фамилию пользователя.
        /// </summary>
        public string LastName { get; set; }

        [Display(Name = "DN_MiddleName", ResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        //[RegularExpression(namesRegex, ErrorMessageResourceName = "RegexMismatch",
            //ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает отчество пользователя.
        /// </summary>
        public string MiddleName { get; set; }

        // Расскомментировать при необходимости
        /*public virtual ICollection<CommentsModel> Comments { get; set; }

        public virtual ICollection<BooksModel> Books { get; set; }

        public virtual ICollection<RecentAttemptsReservationModel> RecentAttemptsReservationModels { get; set; }*/

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            // Типа "важные данные", добавляем их в список.
            string fullName = FirstName;
            if (!String.IsNullOrEmpty(MiddleName) && !String.IsNullOrWhiteSpace(MiddleName))
            {
                fullName += " " + MiddleName;
            }
            fullName += " " + LastName; 
            userIdentity.AddClaim(new Claim("FullName", fullName));
            // после чего в контроллере можем получить их вот так.
            //var gender = identity.Claims.Where(c => c.Type == ClaimTypes.Gender).Select(c => c.Value).SingleOrDefault();
            //var age = identity.Claims.Where(c => c.Type == ClaimTypes.).Select(c => c.Value).SingleOrDefault();
            return userIdentity;
        }
    }
}