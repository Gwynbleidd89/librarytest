﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace LibraryTest.Models
{
    [Table("Books")]
    public class BooksModel
    {

        [Display(Name = "DN_BookID", ResourceType = typeof(Resources.Resource))]
        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        /// <summary>
        /// Задает или возвращает идентификатор книги.
        /// </summary>
        public int Id { get; set; }

        [Display(Name = "DN_Name", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Name",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 150, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает наименование книги.
        /// </summary>
        public string Name { get; set; }

        [Display(Name = "DN_Author", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Author",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 150, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает автора книги.
        /// </summary>
        public string Author { get; set; }

        [Display(Name = "DN_Ganre", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Ganre",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает жанр книги.
        /// </summary>
        public string Ganre { get; set; }

        [Display(Name = "DN_Publisher", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Publisher",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или издателя книги.
        /// </summary>
        public string Publisher { get; set; }

        /// <summary>
        /// Задает или возвращает время окончания бронирования.
        /// </summary>
        public DateTime? ReservationEnd { get; set; }

        [Display(Name = "DN_UsersBookID", ResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает идентификатор бронирующего или получающего книгу пользователя.
        /// </summary>
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        [Required]
        [Display(Name = "DN_IssuingStatus", ResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает статус выдачи.
        /// </summary>
        public bool IssuingStatus { get; set; }

        public virtual ICollection<CommentsModel> Comments { get; set; }

        public virtual ICollection<RecentAttemptsReservationModel> RecentAttemptsReservationModels { get; set; }

    }

}