﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LibraryTest.Models
{
    [Table("RecentAttemptsReservationModels")]
    public class RecentAttemptsReservationModel
    {
        //P.S. С конструктором не работает foreach?!
        /*public RecentAttemptsReservationModels(string UserId, int BookId, DateTime Attempt_DateTime)
        {
            ApplicationUserId = UserId;
            BooksId = BookId;
            AttemptDateTime = Attempt_DateTime;
        }*/

        [Key, Required, Column(Order = 1)]
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        [Key, Required, Column(Order = 2)]
        public int BooksId { get; set; }
        public virtual BooksModel Books { get; set; }

        [Required]
        public DateTime AttemptDateTime { get; set; }
    }
}