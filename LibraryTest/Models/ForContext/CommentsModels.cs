﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryTest.Models
{
    [Table("Comments")]
    public class CommentsModel
    {
        [Display(Name = "DN_CommentedBookID", ResourceType = typeof(Resources.Resource))]
        [Required, Key, Column(Order = 1)]
        public int BooksId { get; set; }
        public BooksModel Books { get; set; }

        [Display(Name = "DN_CommentingUserID", ResourceType = typeof(Resources.Resource))]
        [Required, Key, Column(Order = 2)]
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        [Display(Name = "DN_CommentDate", ResourceType = typeof(Resources.Resource))]
        [DataType(DataType.Date), Required]
        public DateTime CommentDate { get; set; }

        [Display(Name = "DN_Rating", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Rating",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        public int? Rating { get; set; }

        [Display(Name = "DN_Text", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "R_Text",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        [StringLength(maximumLength: 500, MinimumLength = 2, ErrorMessageResourceName = "InvalidateLenght",
            ErrorMessageResourceType = typeof(Resources.Resource))]
        /// <summary>
        /// Задает или возвращает текст комментария.
        /// </summary>
        public string Text { get; set; }

    }
}