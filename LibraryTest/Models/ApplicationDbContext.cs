﻿using LibraryTest.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LibraryTest.Models
{

    public class ApplicationDbContext : IdentityDbContext
    {
        /*public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }*/

        public ApplicationDbContext()
            : base(nameOrConnectionString: "DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<BooksModel> Books { get; set; }
        public DbSet<CommentsModel> Comments { get; set; }
        public DbSet<RecentAttemptsReservationModel> RecentAttemptsReservationModels { get; set; }

    }
}