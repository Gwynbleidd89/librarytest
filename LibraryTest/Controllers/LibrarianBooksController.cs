﻿using LibraryTest.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LibraryTest.Controllers
{
    [Authorize(Roles = "Librarian")]
    public class LibrarianBooksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: LibrarianBooks/CancelReservation/5
        /// <summary>
        /// Отменяет бронь книги.
        /// </summary>
        /// <param name="id">Идентификатор книги, бронь которой необходимо отменить.</param>
        /// <returns>Возвращает Json результат: Success, Message, MessageType</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CancelReservation(int? id)
        {
            if (id == null)
            {
                return Json(new { Message = "Ошибка, пустой идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            if (!(id is int)) //(id.GetType() != typeof(int))
            {
                return Json(new { Message = "Ошибка, недопустимый идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            try
            {
                BooksModel book = await db.Books.FindAsync(id);
                if (book == null)
                {
                    return Json(new { Message = "Ошибка, книга не найдена.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
                }
                if (book.ApplicationUser == null && book.ApplicationUserId == null)
                {
                    return Json(new { Message = "Книга не является забронированной.", MessageType = "info" });
                }
                if (book.IssuingStatus)
                {
                    return Json(new { Message = "Книга имеет статус выданной. Необходимо сначала вернуть книгу в библиотеку.", MessageType = "info" });
                }
                book.ApplicationUserId = null;
                book.ApplicationUser = null;
                book.ReservationEnd = null;
                await db.SaveChangesAsync();
                await GeneralMethods.SendEmailRecentAttemptsReservationAsync(book, db);
                return Json(new { Message = "Бронь была отменена.", MessageType = "success" });
            }
            catch (Exception ex)
            {
                return Json(
                    new {
                        Message = "Исключение: " + ex.Message + " Внутреннее исключение: " + ex.InnerException.Message,
                        MessageType = "error"
                    },
                    JsonRequestBehavior.DenyGet
                );
            }
        }

        // POST: LibrarianBooks/ReturnBook/5
        /// <summary>
        /// Задает книге статус выдачи: "в библиотеке", т.е false.
        /// </summary>
        /// <param name="id">Идентификатор книги, бронь которой необходимо задать статус.</param>
        /// <returns>Возвращает Json результат: Success, Message, MessageType.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ReturnBook(int? id)
        {
            if (id == null)
            {
                return Json(new { Message = "Ошибка, пустой идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            if (!(id is int)) //(id.GetType() != typeof(int))
            {
                return Json(new { Message = "Ошибка, недопустимый идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            try
            {
                BooksModel book = await db.Books.FindAsync(id);
                if (book == null)
                {
                    return Json(new { Message = "Ошибка, книга не найдена.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
                }
                if (!book.IssuingStatus)
                {
                    return Json(new { Message = "Книга уже числится в библиотеке.", MessageType = "info" });
                }
                book.IssuingStatus = false;
                book.ApplicationUserId = null;
                book.ApplicationUser = null;
                book.ReservationEnd = null;
                await db.SaveChangesAsync();
                await GeneralMethods.SendEmailRecentAttemptsReservationAsync(book, db);
                return Json(new { Message = "Книге задан статус - в библиотеке. Бронь сброшена.", MessageType = "success" });
            }
            catch (Exception ex)
            {
                return Json(
                    new {
                        Message = "Исключение: " + ex.Message + " Внутреннее исключение: " + ex.InnerException.Message,
                        MessageType = "error"
                    },
                    JsonRequestBehavior.DenyGet
                );
            }
        }

        // POST: LibrarianBooks/IssueBook/5
        /// <summary>
        /// Выдает забронированную книгу.
        /// </summary>
        /// <param name="id">Идентификатор книги, которую необходимо выдать.</param>
        /// <returns>Возвращает Json результат: Success, Message, MessageType</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> IssueBook(int? id)
        {
            if (id == null)
            {
                return Json(new { Message = "Ошибка, пустой идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            if (!(id is int)) //(id.GetType() != typeof(int))
            {
                return Json(new { Message = "Ошибка, недопустимый идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            try
            {
                BooksModel book = await db.Books.FindAsync(id);
                if (book == null)
                {
                    return Json(new { Message = "Ошибка, книга не найдена.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
                }
                if (book.ApplicationUser == null && book.ApplicationUserId == null)
                {
                    return Json(new { Message = "Книга не является забронированной.", MessageType = "info" });
                }
                book.IssuingStatus = true;
                await db.SaveChangesAsync();
                return Json(new { Message = "Книга была выдана.", MessageType = "success" });
            }
            catch (Exception ex)
            {
                return Json(
                    new {
                        Message = "Исключение: " + ex.Message + " Внутреннее исключение: " + ex.InnerException.Message,
                        MessageType = "error"
                    },
                    JsonRequestBehavior.DenyGet
                );
            }
        }

        // GET: LibrarianBooks
        public async Task<ActionResult> Index([Bind(Include = "FilterValue, FilterParameter, ReservationStatusParameter")] LibrarianBooksViewModel model)
        {
            IQueryable<BooksModel> books = db.Books.Include(b => b.ApplicationUser);
            if (!String.IsNullOrEmpty(model.FilterValue) && !String.IsNullOrWhiteSpace(model.FilterValue))
            {
                switch (model.FilterParameter)
                {
                    case LibrarianBooksViewModel.FilterParameters.Author:
                        books = books.Where(b => b.Author.Contains(model.FilterValue));
                        break;
                    case LibrarianBooksViewModel.FilterParameters.Genre:
                        books = books.Where(b => b.Ganre.Contains(model.FilterValue));
                        break;
                    case LibrarianBooksViewModel.FilterParameters.Publisher:
                        books = books.Where(b => b.Publisher.Contains(model.FilterValue));
                        break;
                    case LibrarianBooksViewModel.FilterParameters.Name:
                        books = books.Where(b => b.Name.Contains(model.FilterValue));
                        break;
                    case LibrarianBooksViewModel.FilterParameters.User_FullName_Or_Email:
                        books = books.Where(b => b.ApplicationUser.Email.Contains(model.FilterValue) 
                            || (b.ApplicationUser.FirstName + " " + b.ApplicationUser.MiddleName 
                                + " " + b.ApplicationUser.LastName).Contains(model.FilterValue));
                        break;
                }
            }
            switch (model.ReservationStatusParameter)
            {
                case LibrarianBooksViewModel.ReservationStatusParameters.Available:
                    books = books.Where(b => b.ApplicationUser == null && b.ApplicationUserId == null);
                    break;
                case LibrarianBooksViewModel.ReservationStatusParameters.Reserved:
                    books = books.Where(b => b.ApplicationUser != null && b.ApplicationUserId != null);
                    break;
                case LibrarianBooksViewModel.ReservationStatusParameters.Issued:
                    books = books.Where(b => b.ApplicationUser != null && (b.ApplicationUserId != null || b.IssuingStatus == true));
                    break;
            }
            model.Books = await books.ToListAsync();
            return View(model);
        }

        // GET: LibrarianBooks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Books/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(LibrarianCreateEditBooksViewModel model)
        { 
            if (ModelState.IsValid)
            {
                BooksModel book = new BooksModel
                {
                    Name = model.Name,
                    Author = model.Author,
                    ApplicationUserId = null,
                    ReservationEnd = null,
                    Ganre = model.Ganre,
                    Publisher = model.Publisher,
                    IssuingStatus = false
                };
                db.Books.Add(book);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: LibrarianBooks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BooksModel books = await db.Books.FindAsync(id);
            if (books == null)
            {
                return HttpNotFound();
            }
            LibrarianCreateEditBooksViewModel model = new LibrarianCreateEditBooksViewModel
            {
                Id = books.Id,
                Author = books.Author,
                Ganre = books.Ganre,
                Publisher = books.Publisher,
                Name = books.Name
            };
            return View(model);
        }

        // POST: LibrarianBooks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(LibrarianCreateEditBooksViewModel model)
        {
            if (ModelState.IsValid)
            {
                BooksModel book = await db.Books.FindAsync(model.Id);
                db.Entry(book).State = EntityState.Modified;
                book.Name = model.Name;
                book.Author = model.Author;
                book.Ganre = model.Ganre;
                book.Publisher = model.Publisher;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: LibrarianBooks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BooksModel books = await db.Books.FindAsync(id);
            if (books == null)
            {
                return HttpNotFound();
            }
            return View(books);
        }

        // POST: LibrarianBooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            BooksModel books = await db.Books.FindAsync(id);
            db.Books.Remove(books);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}