﻿using LibraryTest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace LibraryTest.Controllers
{
    [Authorize(Roles = "JustUser")]
    public class JustUserBooksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: JustUserBooks/CancelReservation/5
        /// <summary>
        /// Отменяет бронь книги.
        /// </summary>
        /// <param name="id">Идентификатор книги, бронь которой необходимо отменить.</param>
        /// <returns>Возвращает Json результат: Success, Message, MessageType</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CancelReservation(int? id)
        {
            if (id == null)
            {
                return Json(new { Message = "Ошибка, пустой идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            if (!(id is int)) //(id.GetType() != typeof(int))
            {
                return Json(new { Message = "Ошибка, недопустимый идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            try
            {
                BooksModel book = await db.Books.FindAsync(id);
                if (book == null)
                {
                    return Json(new { Message = "Ошибка, книга не найдена.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
                }
                string UserId = User.Identity.GetUserId();
                if (UserId == null)
                {
                    return Json(
                        new {
                            Message = "Ошибка, вы не являетесь авторизованным пользователем.",
                            MessageType = "error"
                        }, 
                        JsonRequestBehavior.DenyGet
                    );
                }
                if (book.ApplicationUserId != UserId)
                {
                    return Json(new { Message = "Книга не была вами забронирована.", MessageType = "info" });
                }
                if (book.IssuingStatus == true)
                {
                    return Json(new { Message = "Бронь не была отменена, книга имеет статус выданной, сначала сдайте книгу в библиотеку.", MessageType = "info" });
                }
                book.ApplicationUserId = null;
                book.ApplicationUser = null;
                book.ReservationEnd = null;
                await db.SaveChangesAsync();
                await GeneralMethods.SendEmailRecentAttemptsReservationAsync(book, db);
                return Json(new { Message = "Бронь была отменена.", MessageType = "success" });
            }
            catch (Exception ex)
            {
                return Json(
                    new {
                        Message = "Исключение: " + ex.Message + " Внутреннее исключение: " + ex.InnerException.Message,
                        MessageType = "error"
                    },
                    JsonRequestBehavior.DenyGet
                );
            }
        }
        
        // POST: JustUserBooks/ReserveBook/5
        /// <summary>
        /// Бронирует книгу.
        /// </summary>
        /// <param name="id">Идентификатор книги, которой необходимо забронировать.</param>
        /// <returns>Возвращает Json результат: Success, Message, MessageType</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ReserveBook(int? id)
        {            
            if (id == null)
            {
                return Json(new { Message = "Ошибка, пустой идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            if (!(id is int)) //(id.GetType() != typeof(int))
            {
                return Json(new { Message = "Ошибка, недопустимый идентификатор книги.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            try
            {
                BooksModel book = await db.Books.FindAsync(id);
                if (book == null)
                {
                    return Json(new { Message = "Ошибка, книга не найдена.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
                }
                string UserId = User.Identity.GetUserId();
                if (UserId == null)
                {
                    return Json(
                        new {
                            Message = "Не удалось забронировать книгу, так как только авторизованные пользователи имеют доступ к бронированию.",
                            MessageType = "info"
                        }, 
                        JsonRequestBehavior.DenyGet
                    );
                }
                if (book.ApplicationUser == null && book.ApplicationUserId == null)
                {
                    int MaxReserveCount = 3;
                    if (await db.ApplicationUsers.Where(a => a.Id == UserId && a.VerifiedByAdmin).CountAsync() == 0)
                    {
                        MaxReserveCount = 6;
                    }
                    if (await db.Books.Where(b => b.ApplicationUserId == UserId).CountAsync() < MaxReserveCount)
                    {
                        book.ApplicationUserId = UserId;
                        book.ReservationEnd = DateTime.Now.AddDays(3);
                        await db.SaveChangesAsync();
                        return Json(new { Message = "Книга забронирована за вами на три дня, после истечения срока бронь будет аннулирована!", MessageType = "success" }, JsonRequestBehavior.DenyGet);
                    }
                    else
                    {
                        return Json(
                            new {
                                Message = "Не удалось забронировать книгу, так как превышен лимит. Допускается бронирование только " + MaxReserveCount.ToString() + " книг единовременно.",
                                MessageType = "info"
                            },
                            JsonRequestBehavior.DenyGet
                        );
                    }
                }
                else if (book.ApplicationUserId != UserId)
                {
                    if (await db.RecentAttemptsReservationModels.FindAsync(UserId, book.Id) == null)
                    {
                        db.RecentAttemptsReservationModels.Add(new RecentAttemptsReservationModel { ApplicationUserId = UserId, BooksId = book.Id, AttemptDateTime = DateTime.Now });
                        await db.SaveChangesAsync();
                    } 
                    return Json(
                        new {
                            Message = "Книга уже является забронированной. Мы обязательно уведомим вас по email, когда она освободится.",
                            MessageType = "info"
                        },
                        JsonRequestBehavior.DenyGet
                    );
                }
                else
                {
                    return Json(new { Message = "Книга уже забронированна за вами.", MessageType = "info" }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                return Json(
                    new {
                        Message = "Исключение: " + ex.Message + " Внутреннее исключение: " + ex.InnerException.Message,
                        MessageType = "error"
                    },
                    JsonRequestBehavior.DenyGet
                );
            }
        }

        // GET: JustUserBooks
        public async Task<ActionResult> Index([Bind(Include = "FilterValue, FilterParameter, ReservationStatusParameter")] JustUsersBooksViewModel model)
        {
            IQueryable<BooksModel> books = db.Books.Include(b => b.ApplicationUser);
            if (!String.IsNullOrEmpty(model.FilterValue) && !String.IsNullOrWhiteSpace(model.FilterValue))
            {
                switch (model.FilterParameter)
                {
                    case JustUsersBooksViewModel.FilterParameters.Author:
                        books = books.Where(b => b.Author.Contains(model.FilterValue));
                        break;
                    case JustUsersBooksViewModel.FilterParameters.Genre:
                        books = books.Where(b => b.Ganre.Contains(model.FilterValue));
                        break;
                    case JustUsersBooksViewModel.FilterParameters.Publisher:
                        books = books.Where(b => b.Publisher.Contains(model.FilterValue));
                        break;
                    case JustUsersBooksViewModel.FilterParameters.Name:
                        books = books.Where(b => b.Name.Contains(model.FilterValue));
                        break;
                }              
            }
            string UserId = User.Identity.GetUserId();
            switch (model.ReservationStatusParameter)
            {
                case JustUsersBooksViewModel.ReservationStatusParameters.Available:
                    books = books.Where(b => b.ApplicationUser == null && b.ApplicationUserId == null);
                    break;
                case JustUsersBooksViewModel.ReservationStatusParameters.Reserved:
                    books = books.Where(b => b.ApplicationUser != null && b.ApplicationUserId != null);
                    break;
                case JustUsersBooksViewModel.ReservationStatusParameters.ReservedForMe:
                    books = books.Where(b => b.ApplicationUser != null && b.ApplicationUserId == UserId);
                    break;
                case JustUsersBooksViewModel.ReservationStatusParameters.IssuedToMe:
                    books = books.Where(b => b.ApplicationUser != null && b.ApplicationUserId == UserId && b.IssuingStatus == true);
                    break;
            }
            model.Books = await books.ToListAsync();
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: JustUserBooks/Details
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BooksModel book = null;

            book = await db.Books.FindAsync(id);


            if (book == null)
            {
                return HttpNotFound();
            }

            string UserId = User.Identity.GetUserId();
            CommentsModel comment = await db.Comments.FindAsync(book.Id, UserId);

            // IQueryable не может быть навигационным свойством, он поддерживает lazy loading, но при обращении как к свойству модели не загружается.
            // IQueryable<CommentsModel> как свойство модели будет означать, что в него можно загрузить любые данные, даже не связанные.
            //IQueryable<CommentsModel> comments = db.Comments.Include(c => c.ApplicationUser).Where(c => c.BooksId == book.Id); // && c.ApplicationUserId != UserId); Общая оценка рассчитается без пользовательской
            //Double? doubleRating = (Double?)await comments.SumAsync(c => c.Rating) / await comments.CountAsync();

            Double? doubleRating = (Double?)book.Comments.Sum(c => c.Rating) / book.Comments.Count;
            string stringRating = (doubleRating.HasValue && doubleRating > 0) ? doubleRating.ToString() : "Не определена";

            BookDetailsModel bookDetailsModel = new BookDetailsModel
            {
                Book = book,
                CurrentUserComment = comment,
                GeneralRating = stringRating,
                //AllComments = await сomments.ToListAsync()
                AllComments = book.Comments.ToList()
            };
            bookDetailsModel.AllComments.Remove(comment);
            return View(bookDetailsModel);
        }

        // GET: JustUserBooks/CreateComment
        [HttpGet]
        public async Task<ActionResult> CreateComment(int? BookId)
        {
            if (BookId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BooksModel book = await db.Books.FindAsync(BookId);
            if (book == null)
            {
                return HttpNotFound();
            }
            CommentCreateViewModel commentCreateViewModel = new CommentCreateViewModel();
            return View(commentCreateViewModel);
        }

        // POST: JustUserBooks/CreateComment
        [HttpPost, ActionName("CreateComment")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateComment(CommentCreateViewModel model, int BookId)
        {
            if (ModelState.IsValid)
            {
                string UserId = User.Identity.GetUserId();
                if (String.IsNullOrEmpty(UserId)) 
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                CommentsModel comment = new CommentsModel
                {
                    BooksId = BookId,
                    ApplicationUserId = UserId,
                    CommentDate = DateTime.Now,
                    Rating = (int)model.RatingParameter,
                    Text = model.CommentText
                };
                db.Comments.Add(comment);
                await db.SaveChangesAsync();
                return RedirectToAction("Details/" + BookId.ToString());
            }
            return View(model);
        }

    }
}