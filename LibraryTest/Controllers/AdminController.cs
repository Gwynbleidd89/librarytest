﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibraryTest.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Security;

namespace LibraryTest.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: Admin/AdminConfirm
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AdminConfirm(string id)
        {
            if (id == null)
            {
                return Json(new { Message = "Ошибка, пустой идентификатор пользователя.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            if (!(id is string)) //(id.GetType() != typeof(int))
            {
                return Json(new { Message = "Ошибка, недопустимый идентификатор пользователя.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            try
            {
                ApplicationUser user = await db.ApplicationUsers.FindAsync(id);
                if (user == null)
                {
                    return Json(new { Message = "Ошибка, пользователь не найден.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
                }
                string UserId = User.Identity.GetUserId();
                if (UserId == null || !User.IsInRole("Admin"))
                {
                    return Json(
                        new
                        {
                            Message = "Ошибка, вы не являетесь авторизованным пользователем.",
                            MessageType = "error"
                        },
                        JsonRequestBehavior.DenyGet
                    );
                }
                if (user.VerifiedByAdmin == true)
                {
                    return Json(new { Message = "Пользовательская учетная запись уже является подтвержденной.", MessageType = "info" });
                }
                user.VerifiedByAdmin = true;
                await db.SaveChangesAsync();
                return Json(new { Message = "Учетная запись была подтверждена.", MessageType = "success" });
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        Message = "Исключение: " + ex.Message + " Внутреннее исключение: " + ex.InnerException.Message,
                        MessageType = "error"
                    },
                    JsonRequestBehavior.DenyGet
                );
            }
        }

        // POST: Admin/AdminCancelConfirm
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AdminCancelConfirm(string id)
        {
            if (id == null)
            {
                return Json(new { Message = "Ошибка, пустой идентификатор пользователя.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            if (!(id is string)) //(id.GetType() != typeof(int))
            {
                return Json(new { Message = "Ошибка, недопустимый идентификатор пользователя.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
            }
            try
            {
                ApplicationUser user = await db.ApplicationUsers.FindAsync(id);
                if (user == null)
                {
                    return Json(new { Message = "Ошибка, пользователь не найден.", MessageType = "error" }, JsonRequestBehavior.DenyGet);
                }
                string UserId = User.Identity.GetUserId();
                if (UserId == null || !User.IsInRole("Admin"))
                {
                    return Json(
                        new
                        {
                            Message = "Ошибка, вы не являетесь авторизованным пользователем.",
                            MessageType = "error"
                        },
                        JsonRequestBehavior.DenyGet
                    );
                }
                if (user.VerifiedByAdmin == false)
                {
                    return Json(new { Message = "Пользовательская запись уже имеет статус неподтвержденной.", MessageType = "info" });
                }
                user.VerifiedByAdmin = false;
                await db.SaveChangesAsync();
                return Json(new { Message = "Подтверждение учетной записи было отменено.", MessageType = "success" });
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        Message = "Исключение: " + ex.Message + " Внутреннее исключение: " + ex.InnerException.Message,
                        MessageType = "error"
                    },
                    JsonRequestBehavior.DenyGet
                );
            }
        }

        // GET: Admin
        public async Task<ActionResult> Index([Bind(Include = "FilterValue")] UsersViewModel model)
        {
            IQueryable<ApplicationUser> applicationUsers = db.ApplicationUsers;
            if (!String.IsNullOrEmpty(model.FilterValue) && !String.IsNullOrWhiteSpace(model.FilterValue))
            {
                applicationUsers = applicationUsers.Where(a => a.Email.Contains(model.FilterValue) 
                    || (a.FirstName + " " + a.MiddleName + " " + a.LastName).Contains(model.FilterValue));
            }
            string UserId = User.Identity.GetUserId();
            model.ApplicationUsers = await applicationUsers.Where(a => a.Id != UserId).ToListAsync();         
            return View(model);
        }

        // GET: Admin/Create
        public async Task<ActionResult> Create()
        {
            CreateEditUserViewModel createUserViewModel = new CreateEditUserViewModel();

            List<IdentityRole> roles = await db.Roles.ToListAsync();
            List<string> rolesNames = new List<string>();

            foreach (IdentityRole role in roles)
            {
                rolesNames.Add(role.Name);
            }

            createUserViewModel.Role = new SelectList(rolesNames);
            return View(createUserViewModel);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        // POST: Admin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Email, FirstName, LastName, MiddleName, RoleValue, Role")] CreateEditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser applicationUser = new ApplicationUser
                {
                    Email = model.Email,
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    MiddleName = model.MiddleName,
                    LastName = model.LastName,
                    EmailConfirmed = true,
                    VerifiedByAdmin = true
                };

                UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

                string userPWD = Membership.GeneratePassword(15, 1);
    
                var result = await UserManager.CreateAsync(applicationUser, userPWD);
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(applicationUser.Id, model.RoleValue.ToString());
                    EmailService emailService = new EmailService();
                    UserManager.EmailService = emailService;
                    await UserManager.SendEmailAsync(applicationUser.Id, "Аккаунт в библиотеке",
                        "Ваша учетная запись создана администратором, сгенерированный пароль: " + userPWD);
                    UserManager.Dispose();
                    return View("AfterUserCreatingMessage");
                }
                UserManager.Dispose();
                AddErrors(result);
            }

            CreateEditUserViewModel createUserViewModel = new CreateEditUserViewModel();
            List<IdentityRole> roles = await db.Roles.ToListAsync();
            List<string> rolesNames = new List<string>();

            foreach (IdentityRole role in roles)
            {
                rolesNames.Add(role.Name);
            }

            createUserViewModel.Role = new SelectList(rolesNames);
            return View(createUserViewModel);
        }

        // GET: Admin/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            // Получаем все роли
            List<IdentityRole> roles = await db.Roles.ToListAsync();
            List<string> rolesNames = new List<string>();

            bool roleIsGet = false;
            
            string DefaultRole = "";

            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            // Получаем названия всех ролей
            for (int Index = 0; Index < roles.Count; Index++)
            {
                // Получаем индекс роли в списке имен ролей 
                if (await UserManager.IsInRoleAsync(applicationUser.Id, roles[Index].Name) && !roleIsGet)
                {
                    DefaultRole = roles[Index].Name;
                    roleIsGet = true;
                }
                rolesNames.Add(roles[Index].Name);
            }

            UserManager.Dispose();

            CreateEditUserViewModel editUserViewModel = new CreateEditUserViewModel
            {
                UserID = applicationUser.Id,
                Email = applicationUser.Email,
                FirstName = applicationUser.FirstName,
                MiddleName = applicationUser.MiddleName,
                LastName = applicationUser.LastName,
                Role = new SelectList(rolesNames, DefaultRole)
            };            

            return View(editUserViewModel);
        }

        // POST: Admin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserID, Email, FirstName, LastName, MiddleName, RoleValue, Role")] CreateEditUserViewModel editUserViewModel)
        {
            if (await db.ApplicationUsers.Where(a => a.Email == editUserViewModel.Email && a.Id != editUserViewModel.UserID).CountAsync() > 0)
            {
               AddErrors(new IdentityResult(new string[] { "Пользователь с такой электронной почтой уже существует." }));
            }
            else if (ModelState.IsValid)
            {
                ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(editUserViewModel.UserID);

                applicationUser.Email = editUserViewModel.Email;
                applicationUser.FirstName = editUserViewModel.FirstName;
                applicationUser.MiddleName = editUserViewModel.MiddleName;
                applicationUser.LastName = editUserViewModel.LastName;

                UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

                // Получили все роли пользователя
                IList<string> userRoles = await UserManager.GetRolesAsync(editUserViewModel.UserID);

                // Удалили все роли пользователя
                await UserManager.RemoveFromRolesAsync(editUserViewModel.UserID, userRoles.ToArray());

                // Назначили пользователю выбранную роль
                await UserManager.AddToRoleAsync(editUserViewModel.UserID, editUserViewModel.RoleValue);

                // Сохранили данные пользователя
                db.Entry(applicationUser).State = EntityState.Modified;
                await db.SaveChangesAsync();

                // Сбросили и установили пароль
                string userPWD = Membership.GeneratePassword(15, 1);
                await UserManager.RemovePasswordAsync(applicationUser.Id);
                await UserManager.AddPasswordAsync(applicationUser.Id, userPWD);


                EmailService emailService = new EmailService();
                UserManager.EmailService = emailService;
                await UserManager.SendEmailAsync(applicationUser.Id, "Аккаунт в библиотеке",
                        "Ваша учетная запись была изменена администратором, сгенерированный пароль: " + userPWD);

                UserManager.Dispose();

                return View("AfterUserCreatingMessage");
            }

            List<IdentityRole> roles = await db.Roles.ToListAsync();
            List<string> rolesNames = new List<string>();

            foreach (IdentityRole role in roles)
            {
                rolesNames.Add(role.Name);
            }

            editUserViewModel.Role = new SelectList(rolesNames, editUserViewModel.RoleValue);

            return View(editUserViewModel);
        }

        // GET: Admin/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(id);
            db.ApplicationUsers.Remove(applicationUser);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
